import time
from collections import deque
from openpyxl import load_workbook
from openpyxl.cell import column_index_from_string as col_index


INIT_PROGRAM_TIME = time.clock()

def pos_init(ws):
	for row in ws.rows:
		for cell in row:
			if cell.value == 's':
				start_cell = cell
			if cell.value == 'f':
				finish_cell = cell
			if cell.border.left.style == cell.border.top.style == 'medium':
				board_start = cell
			if cell.border.right.style == cell.border.bottom.style == 'medium':
				board_end = cell
	return start_cell, finish_cell, board_start, board_end

def bfs(start_cell, finish_cell, board_start, board_end):
	checked = set()
	queue = deque([start_cell])
	branch = deque([[start_cell]])

	pos_steps = [(1,2),(2,1),(2,-1),(1,-2),(-1,-2),(-2,-1),(-2,1),(-1,2)]

	while queue:

		node = queue.popleft()
		path = branch.popleft()

		if node not in checked:
			checked.add(node)

			for step in pos_steps:
				if col_index(node.column) + step[0] > 0 and node.row + step[1] > 0:
					step = node.offset(step[0], step[1])

				if step.fill.fgColor.rgb != 'FFFF0000' and \
					col_index(board_start.column) <= col_index(step.column) <= col_index(board_end.column) and \
					board_start.row <= step.row <= board_end.row:

					copy_path = path[:] 
					copy_path.append(step) 
					branch.append(copy_path)

					if step == finish_cell:
						return branch[-1]
						
					queue.append(step)


def print_path(path, letter):
	for step in path:
		step.value = letter
	print 'Success.'

def main():
	wb = load_workbook('horse_input.xlsx')
	ws = wb.active

	found_init = pos_init(ws)

	ALG_TIME = time.clock()
	ALGORITHM = bfs(*found_init)
	ws['AQ2'].value = str(round(time.clock() - ALG_TIME, 3)) + ' sec'
	print "Algorithm time: " + ws['AQ2'].value

	print_path(ALGORITHM, 'g')
	ws['S2'].value = str(round(time.clock() - INIT_PROGRAM_TIME, 3)) + ' sec'
	print "Total time: " + ws['S2'].value
	wb.save('horse_output.xlsx')

if __name__ == '__main__':
	main()
